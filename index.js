const express = require("express");
const mongoose = require("mongoose");




const app = express();

const port = 4000

//mongoos connection
mongoose.connect("mongodb+srv://adrian:adrian@cluster0.1nbs0.mongodb.net/task152?retryWrites=true&w=majority",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	});

//We will create notifications if the connection to the db is success or failed

let db = mongoose.connection;
//We add this so that when db has an error, we will show the connection error
//in both the terminal and in the browser for our client.
db.on('error',console.error.bind(console, "connection error."))

//Once the connection is open and successful, we will output a message in
//the terminal/gitbash.
db.once('open',()=>console.log("connected to MongoDB"))

//middleware- in expressjs, are methods functions that acts and adds features 
//to our application
//express.json() - handle the request body of request. It handles the JSON data from 
//our client.
app.use(express.json());

//mongoose schema
//Before we can create docs from our api to save into our db, we first have to
//determine the structure of the docs to be written in the db.
//schema acts as a blueprint for our data/document.
//a schema is a representation of how the doc is structured. It also determins
//the types of data and the expected properties. Gone are the days where we have to worry
//if you input "stock" as fields. Schemas allow us disallow us to create documents
//which does not follow the schema:


//schema() is a constructor from mongoose that will allow us to create a new schema object.

const taskRoutes = require('./routes/taskRoutes');
//Our server will use a middleware to group all task routes under /tasks.
//meaning to say, all the endpoints in taskRoutes file will start with /tasks.
app.use('/tasks',taskRoutes);

const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes);






app.listen(port,()=>console.log(`Server is running at port ${port}`));
