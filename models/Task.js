const mongoose = require("mongoose");

/*
	The name convention for files, are singular and capitalized names that describe
	the schema

*/

const taskSchema = new mongoose.Schema({

/*
	Define the fields for the task document.
	The task doc should ahve a name file nad a status field.
	Both fields must be strings.
*/

	name: String,
	status: String

});


//Mongoose model

/*
	Models are used to connect your api to the coresponding collection in your
	db. It is a representation of the task documents.

	Models uses schemas to create objects that correspond to the schema. By default,
	when creating the collection from your model, the collection name is pluralized.

	mongoose.model(<nameOFCollectionInAtlas>,<schemaToFollow>)
*/

module.exports = mongoose.model("Task",taskSchema);