const Task = require("../models/Task");

module.exports.createTaskController = (req,res) => {

	console.log(req.body);

	Task.findOne({name:req.body.name})
	.then(result => {

		if(result !== null && result.name === req.body.name){
			return res.send("Duplicate Task Found");
		}else
			{
			let newTask = new Task ({

				name: req.body.name,
				status: req.body.status,
				
			})
			
			newTask.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));

		}

	})	
	.catch(error => res.send(error))
	

	//Task model is a constructor.
	//newtask that will be created from our task model will have additional methods
	//to be used in our application
	


	//.save() is a method from an object created by a model.
	// this will allow us to save the document into the collection.
	//save() can have anonymous fucntion or we can have a then chain.
	//The anonymous function in the save() method is used to handle the error
	//or the proper result/response from mongodb.
	//.then() is used to handle the proper result/returned value of a function. If the function
	//properly returns a value, we can run a separate function to handle it.
	//.catch() is used to handle/catch the error from the use of a function. So that 
	//if there is an error, we an properly handle it separate from the result.



}



module.exports.getAllTasksController = (req,res) => {


	Task.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))

};


module.exports.getSingleTaskController = (req, res) => {

	console.log(req.params);

	Task.findById(req.params.id)
	.then(result => res.send(result))
	.catch(error => res.send(error));
}

module.exports.updateTaskStatusController = (req,res) => {

	console.log(req.params.id);
	console.log(req.body);

	let updates = {

		status: req.body.status
	}

	Task.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));

	
}









