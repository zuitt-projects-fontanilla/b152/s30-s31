const express = require("express");

const router = express.Router();



const taskControllers = require('../controllers/taskControllers');

//console.log(taskControllers);

router.post('/',taskControllers.createTaskController);


router.get('/',taskControllers.getAllTasksController);

//URL http//localhost:4000/tasks/getSingleTask
router.get('/getSingleTask/:id',taskControllers.getSingleTaskController);

router.put('/updateTaskStatus/:id',taskControllers.updateTaskStatusController);


module.exports = router;
